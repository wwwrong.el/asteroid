--[[Asteroid version 0.0.1

MIT License

Copyright (c) 2021 Eittichai L.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.]]

local wsrequest = require "wsapi.request"
local wsresponse = require "wsapi.response"
local wsutil = require "wsapi.util"
local concat = table.concat
local insert = table.insert

local M = {}

local function date_util(date)
  local _date = os.time()
  if type(date) == "number" then
    _date = date
  elseif type(date) == "table" then
      _date = os.time(date)
  elseif type(date) == "string" then
    local date_pattern = "(%d%d%d%d)-(%d%d)-(%d%d)%s?(.*)"
    local time_pattern = "(%d%d):(%d%d):(%d%d)"
    local year, month, day, time = date:match(date_pattern)
    if year then
      local date_table = {
        day = day,
        month = month,
        year = year
      }
      if #time >= 8 then
        local hour, min, sec = time:match(time_pattern)
        if hour then
          date_table.hour = hour
          date_table.min = min
          date_table.sec = sec
        end
      end
      _date = os.time(date_table)
    else
      error("Invalid date format (%Y-%m-%d or %Y-%m-%d %H:%M:%S)")
    end
  end
  return function (format)
    format = format or "%Y-%m-%d %H:%M:%S"
    return os.date(format, _date)
  end
end

local function join_path(path1, path2)
  local new_path = ""
  local slash_or_empty = {["/"]=true, [""]=true}
  if type(path1)=="string" and type(path2)=="string" then
    if (slash_or_empty[path1:sub(-1,-1)] or slash_or_empty[path2:sub(1,1)]) then
      new_path = path1 .. path2
    else
      new_path = path1 .. "/" .. path2
    end
  end
  return new_path
end

-- custom หน้าแสดง 500 error ได้ด้วยไฟล์ 500.txt
local function err500(web, err)
  local file, result, output
  err = wsutil.sanitize(err)
  file, _ = io.open("500.txt", "r")
  if file then
    local fn = {"return function ()"}
    for line in file:lines() do
      insert(fn, "coroutine.yield([[" .. line:gsub("{{err}}", "<pre>" .. err .. "</pre>"):gsub("{{(.-)}}", "]] .. %1 .. [[") .. "]])")
    end
    file:close()
    insert(fn, "end")
    local status, output = pcall(load(concat(fn, "\n"), "template500", "t", web))
--     if not status then return err500(web, output) end
    if status then result = output end
  end
  -- ถ้าไม่กำหนด 500.txt จะแสดงข้อผิดพลาดแทน
  if not result then
    if output then
      err = err .. "\n" .. output
    end
    result = function ()
      coroutine.yield("<!DOCTYPE HTML><html><head><meta charset=utf-8><title>Error!!</title></head>")
      coroutine.yield("<body><h1>500 Error</h1>")
      coroutine.yield("<p>An error has occurred while serving this page.</p>")
      coroutine.yield("Error details:<pre>&nbsp;&nbsp;&nbsp;\n" .. err:gsub("\n", "&nbsp;&nbsp;&nbsp;\n") .. "</pre></body>")
    end
  end
  return 500, web.header, coroutine.wrap(result)
end

-- custom หน้าแสดง 404 error ได้ด้วยไฟล์ 404.txt
local function err404(web)
  local file, result
  file, _ = io.open("404.txt", "r")
  if file then
    local fn = {"return function ()"}
    for line in file:lines() do
      insert(fn, "coroutine.yield([[" .. line:gsub("{{(.-)}}", "]] .. %1 .. [[") .. "]])")
    end
    file:close()
    insert(fn, "end")
    status, output = pcall(load(concat(fn, "\n"), "template404", "t", web))
    if not status then return err500(web, output) end
    result = output
  else -- ถ้าไม่กำหนด 404.txt จะใช้ค่า default
    result = function ()
      coroutine.yield("<!DOCTYPE HTML><html><head><meta charset=utf-8><title>Page not found!!</title></head>")
      coroutine.yield("<body><h1>404 Error</h1>")
      coroutine.yield("<p>The requested URL was not found on this server.</p>")
      coroutine.yield("<a href=\"" .. web.script .. "/\">back to home</a></body>")
    end
  end
  return 404, web.header, coroutine.wrap(result)
end

local function response(web, body)
  if web.code ~= 500 then
    if type(body) == "function" then
      co = coroutine.wrap(body)
      status, err = pcall(function ()
        local ret = ""
        while ret do
          ret = co()
        end
      end)
      if status then
        return web.code, web.header, coroutine.wrap(body)
      end
      return err500(web, err)
    end
    if type(body) == "string" then
      return web.code, web.header, coroutine.wrap(function ()
        coroutine.yield(body)
      end)
    end
    return err500(web, "Invalid return type: expected 'function' or 'string' got '" .. type(body) .. "'")
  end
  return err500(web, body)
end

local function save_file(web, path, name)
  if web.params.upload then
    local file, err = io.open(join_path(path, name or web.params.upload.name), "wb")
    print(name, web.params.upload.name)
    if not file then
      return err500(web, err)
    end
    file:write(web.params.upload.contents)
    file:flush()
    file:close()
  end
end

local function set_cookie(web, args)
  local str = ("%s=%s"):format(args.key, args.value)
  if args.expire then
    str = str .. "; Expires=" .. tostring(args.expire)
  end
  web.header["Set-Cookie"] = str
end

local function make_pattern(patt)
  local pattern = {param_keys = {}, pattern = ""}
  patt = patt:gsub("[%$%%%^%*%(%)%-%+%[%.%?]", function (char)
    if char == "*" then
      return ":*"
    else
      return "%" .. char
    end
  end)
  patt = patt:gsub(":([%w%*]+)(/?)", function (param, slash)
    if param == "*" then
      insert(pattern.param_keys, 'splat')
      return "(.-)" .. slash
    else
      insert(pattern.param_keys, param)
      return "([^/?&#]+)" .. slash
    end
  end)
  if patt:sub(-1) ~= "/" then
    patt = patt .. "/"
  end
  pattern.pattern = "^" .. patt .. "?$"
  return pattern
end

local function add_route(app, method, handler, patterns)
  for _, pattern in ipairs(patterns) do
    method = method:upper()
    if not app.route[method] then app.route[method] = {} end
    insert(app.route[method], {
      pattern = make_pattern(pattern),
      handler = handler
    })
  end
end

local function route_method(method)
  return function (app, handler, ...)
    add_route(app, method, handler, {...})
  end
end

local function handle_request(app, env)
  local route = app.route[env.REQUEST_METHOD]
  local path = env.PATH_INFO
  local request = wsrequest.new(env)
  local header = { ["Content-type"] = "text/html" }
  local script_name = env.SCRIPT_NAME
  if route then
    local web = setmetatable({
      code = 200,
      cookies = {},
      header = header,
      params = {},
      sanitize = wsutil.sanitize,
      script = script_name,
      setdate = date_util
    }, { __index = _G })
    web.setcookie = function (args) set_cookie(web, args) end
    web.savefile = function (path, name) save_file(web, path, name) end
    for _, v in ipairs(route) do
      if env.HTTP_COOKIE then
        for k, v in env.HTTP_COOKIE:gmatch("(%w+)%=(%w+)") do
          web.cookies[k] = v
        end
      end
      local param_vals = { path:match(v.pattern.pattern) }
      if #param_vals > 0 then
        for i, p in ipairs(v.pattern.param_keys) do
          if p == 'splat' then
            if not web.params.splat then web.params.splat = {} end
            insert(web.params.splat, wsutil.url_decode(param_vals[i]))
          else
            web.params[p] = param_vals[i]
          end
        end
        for k, p in pairs(request.POST) do
          if not web.params[k] then web.params[k] = p end
        end
        status, output = pcall(v.handler, web)
        if not status then
          return err500(web, output)
        end
        if type(output) == "table" then
          if output.cmd == "redirect" then
            if not output.fullpath then
              output.url = web.script .. output.url
            end
            web.header["Location"] = output.url
            web.code = output.code
            return response(web, "")
          end
        end
        return response(web, output)
      end
    end
    return err404(web)
  end
end

function M.new(option)
  local app = {
    route = {},
    get = route_method("GET"),
    post = route_method("POST"),
    put = route_method("PUT"),
    delete = route_method("DELETE"),
    redirect = function (app, path, is_fullpath)
      if path:sub(1,1) ~= "/" then path = "/" .. path end
      return { cmd = "redirect", code = 302, url = path, fullpath = is_fullpath }
    end,
    redirect_permanent = function (app, path, is_fullpath)
      if path:sub(1,1) ~= "/" then path = "/" .. path end
      return { cmd = "redirect", code = 301, url = path, fullpath = is_fullpath }
    end
  }
  if type(option) == "table" then
    -- TODO
  end
  app.run = function (wsapi_env)
    return handle_request(app, wsapi_env)
  end
  return app
end

return M
