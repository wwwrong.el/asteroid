![asteroid](example/image/logo.png)
# Asteroid

Small and simple Lua web framework

## Why
See [rationale](rationale.md)

## Installation
Copy and paste file `asteroid.lua` in your project.

## Usage
See example

## License
The code is released under the MIT terms.

## Project status
Work in progress...

