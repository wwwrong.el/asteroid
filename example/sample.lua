#!/usr/bin/env wsapi.cgi

local inspect=require"inspect"
local asteroid = require "asteroid"
local app = asteroid.new()

local function response_body(title)
  return [[<!DOCTYPE HTML>
    <html><head><title>]] .. title .. [[</title>
    <meta charset=utf-8><style type="text/css">
    footer{
      background-color: gray;
      clear: both;
      position: relative;
      top: 15px;
      width: 960px;
    }
    body{
      background-color: silver;
    }
    </style></head>
    <body><section>%s</section><footer>some text</footer></body></html>]]
end

app:get(function (_ENV)
  if cookies.user and cookies.user == "user1" then
    local date = setdate("2022-01-01")
    local body = "Hello " .. cookies.user ..
      "<br/><br/>".. date("%c") .. "<br/>header: <pre>" ..
      sanitize(inspect(header)) .. "</pre> +++ <br/>parameter: <pre>" ..
      sanitize(inspect(params)) .. "</pre><br/>cookies: <pre>" ..
      sanitize(inspect(cookies)) .. "</pre><br/>" ..
      '<img src="/image" style="width:128px"><br/>' ..
      [[<form name="up" action="/file" method="POST"enctype="multipart/form-data">
    <input name="upload" type="file" id="myup"><br/>
    Name: <input type="text" name="text1" id="txt">
    <input type="submit"></form><br/>
      <a href="/logout">Logout</a>]]
    return response_body("Hello " .. cookies.user):format(body)
  end
  return app:redirect("/login")
end, "/", "/index")

app:post(function (_ENV)
  local body = "parameter: <pre>" .. sanitize(inspect(params)) .. "</pre><br/>"
  if params.upload and params.upload.name then
    savefile("./example", ((params.text1 .. params.upload.name:match("%.?[_%d%w%.]+(%..+)"))) or params.upload.name)
    body = body .. "upload success.<br/><a href"
  else
    return app:redirect("/show/" .. (params.text1 or "Boby"))
  end
  body = body .. '<a href="/">Back</a>'
  return response_body("File " .. (params.upload and params.upload.name or "")):format(body)
end, "/file")

app:get(function (_ENV)
  local body = [[<h1>Login:</h1><form action="/" method="post">
    <input type="text" id="user" name="user">
    <input type="submit" value="ตกลง">
    </form><br/>user: ]] .. (cookies.user or "")
  return response_body("Login"):format(body)
end, "/login")

app:get(function (_ENV)
  local body = "<h1>แสดง parameter</h1>" ..
    "params: <pre>" .. sanitize(inspect(params)) ..
    "<br/>:name = " .. params.name ..
    '<br/><a href="/">Back</a>'
  return response_body("Show parameter"):format(body)
end, "/show/:name")

app:get(function (_ENV)
  setcookie({key="user", value=params.user, expire=os.date("!%c", 1)})
  return app:redirect("/")
end, "/logout")

app:post(function (_ENV)
  setcookie({key="user", value=params.user})
  return app:redirect("/")
end, "/")

app:get(function (_ENV)
  local image, err = io.open("example/image/asteroid.png", "rb")
  --local image, err = io.open("image/asteroid.png", "rb")
  if image then
    header["Content-type"] = "image/png"
    local img_data = image:read("a")
    image:close()
    return img_data
  end
  code = 500
  return err
end, "/image")

app:get(function (_ENV)
  local body = "<p>params:</p>" .. sanitize(inspect(params)) .. "<br/>"
  return response_body("Splat"):format(body)
end, "/params/*")

-- ฟังก์ชัน สร้าง error
app:get(function(_ENV)
  return function () return "asdf" end
end, "/err")

return app