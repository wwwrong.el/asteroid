-- local hfile = require "xavante.filehandler"
-- local hcgi = require "xavante.cgiluahandler"
-- local hredir = require "xavante.redirecthandler"
local hwsapi = require "wsapi.xavante"
local xavante = require "xavante"
  
-- Define here where Xavante HTTP documents scripts are located
-- local webDir = "./"
local app
if arg[1] then
  app = arg[1]:match("^(.-)%.lua$")
end
if not app then
  app = "index"
end

local simplerules = {
    {
      match = "^/(.-)$",
      with = hwsapi.makeHandler(app)
    }
} 

xavante.HTTP{
    server = {host = "*", port = 8080},
    
    defaultHost = {
    	rules = simplerules
    },
}

xavante.start()